// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
const { ipcRenderer } = require("electron");

let showModal = document.getElementById("show-modal"),
  closeModal = document.getElementById("close-modal"),
  modal = document.getElementById("modal"),
  addItem = document.getElementById("add-item"),
  itemUrl = document.getElementById("url");

showModal.addEventListener("click", e => {
  modal.style.display = "flex";
  itemUrl.focus();
});

closeModal.addEventListener("click", e => {
  modal.style.display = "none";
});

// send item to main after click
addItem.addEventListener("click", e => {
  if (itemUrl.value) {
    console.log(itemUrl.value);

    // send new item to main process
    ipcRenderer.send("new-item", itemUrl.value);
  }
});

// Listen for response from main relating to new item via CHANNEL called 'new-item-success'
ipcRenderer.on("new-item-success", (e, newItem) => {
  console.log(newItem);
});

// Listen for keyboard submit
itemUrl.addEventListener("keyup", e => {
  if (e.key === "Enter") addItem.click();
});
